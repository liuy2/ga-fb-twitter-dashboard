<?php

    define('TOKEN', 'EAASEj4iLMMkBALzi4WAX6aWFM4ysdZBP9ROsPw9M0jqOv5XtpYirmtOoIZB48VQBkWxU9wQTXetYAl1xYjOkjUfzjZAocDZAP5OZCsiZBZAXhSAF2wLAUQDjrjljVZA0fdKaW7EXn1qh6cbk4zXd41V7Dh1wC6OAZALLPEW00FPv4qwZDZD');

    //$date is a php date object
    function getFacebookData($metrics, $date){
        $facebook_data = array();

        //offset for the way Facebook returns data. Apparently its one day late
        $start_time = strtotime($date) + SECONDS_PER_DAY;
        $end_time = strtotime($date) + 2 * SECONDS_PER_DAY;

        foreach($metrics as $m){
            
            $service_url = 'https://graph.facebook.com/v2.8/244892072221776/insights/' . $m . '?access_token='. TOKEN . '&period=day&format=json&method=get&since=' . $start_time . '&until=' . $end_time;
        
            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            
            $curl_response = curl_exec($curl);
            if ($curl_response != false){
                //echo $curl_response;
                $b = json_decode( $curl_response , true);
                $facebook_data[$m] = ($b["data"][0]["values"][0]["value"]);
                $facebook_data["end_time"] = ($b["data"][0]["values"][0]["end_time"]);
            }
            curl_close($curl);
        }

        $facebook_data["date"] = $date;

        return ($facebook_data);
    }
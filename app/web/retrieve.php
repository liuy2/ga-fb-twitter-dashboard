<?php
    //retrieve a given day's data
    //if no date is given, get today's date

    date_default_timezone_set('EST');

    define('SECONDS_PER_DAY', 86400);
    define('DAYS_PRIOR', 3);
    

    define('APP_ID', 'IQKSBFPSWC');
    define('API_KEY', 'c41440f4ceb41c48e343dc31f6f78ac4');
    define('INDEX', 'analytics_dashboard_store');

    require_once 'AlgoliaSearch/algoliasearch.php';
    require 'facebook/facebook.php';
    require 'ga/ga.php';

    header('Content-Type:application/json');

    $days = 0;
    $start = 0;
    $end = 0;
    if($_GET['end'] != null && $_GET['start'] != null){

        $start = strtotime($_GET['start']);
        $end = strtotime($_GET['end']);
        if($start < $end){
            $difference = $end-$start;
            $days = $difference / SECONDS_PER_DAY;
        }
    }

    //read files defining metrics to be read and uploaded
    $metrics = json_decode(file_get_contents('metrics.json'), true);

    $facebook_metrics = $metrics["fb"];
    $ga_metrics = $metrics["ga"]["nodim"];
    $ga_view = $metrics["ga"]["view"];

    
    for($i = 0; $i <= $days; $i++){
        $date = 0;
        if($days == 0){
            $date = date('Y-m-d', (time() - SECONDS_PER_DAY * DAYS_PRIOR));
        } else {
            $date = date('Y-m-d', $start + $i * SECONDS_PER_DAY);
        }
        
        $data = [];
        $data["facebook"] = getFacebookData($facebook_metrics, $date);
        $data["google"] = getGAData($ga_metrics, $date, $ga_view);


        $data["objectID"] = $date;
        echo json_encode($data);
        //echo $date . "----";

        //successful retrieval. write to Algolia
        $client = new \AlgoliaSearch\Client(APP_ID, API_KEY);
        $index = $client->initIndex(INDEX);

        $object = null;

        try{
            $object = $index->getObject($objectID);
        } catch(Exception $e){}

        $res=null;

        if($object == null){
            //no object found. add a new one
            $res = $index->addObject($data);
        } else {
            //update existing object
            $res = $index->partialUpdateObject($data);
        }

        $index->waitTask($res['taskID']);
    }
    
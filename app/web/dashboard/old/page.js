var sumReduceAdd = function(p, v) {
	p.count++;
	return p;
}

var sumReduceRemove = function(p, v) {
	p.count--;
	return p;
}

var sumReduceInitial = function() {
  return {count: 0};  
}

var keywordsDim = null;
var keywordsDimGroup = null;
$(function(){
	var cf = crossfilter(data);
	
	//convert date into d3 readable date
	var parseDate = d3.time.format("%Y-%m-%d").parse;
	
	data.forEach(function(d){
		d.date = parseDate(d.date);
	});

	//bar chart
	keywordsDim = cf.dimension(function(d){ return d.keywords;});
	var cloneKeywordsDim = $.extend(true, {}, keywordsDim);
	keywordsDimGroup = keywordsDim.groupAll().reduce(
		function reduceAdd(p, v) {
		  v.keywords.forEach (function(val, idx) {
			 p[val] = (p[val] || 0) + 1;
		  });
		  return p;
		},
		function reduceRemove(p, v) {
		  v.keywords.forEach (function(val, idx) {
			 p[val] = (p[val] || 0) - 1;
		  });
		  return p;

		},
		function reduceInitial() {
		  return {};
	}).value();

	//find top num amount 
	var topKeywordsGroup = function(x){
		return {
			all: function(){
				//need a deep copy of keywordsDimGroup for easy manipulation
				var clone = $.extend(true, {}, keywordsDimGroup);
				var newDimGroup = [];
				
				var max = 0;
				var mykey = null;
				//iterate through entire group
				for(var i = 0; i < x; i++){
					for (var key in clone) {
						if (clone.hasOwnProperty(key) && key != "all" && clone[key].valueOf() > max) {
							mykey = key;
							max = clone[key].valueOf();
						}
					}

					newDimGroup.push({
						key: mykey,
						value: max
					});
					
					delete clone[mykey];
					
					max = 0;
				}
				
				return newDimGroup;
			}
		};
	};

	//line chart
	var dateDim = cf.dimension(function(d){ return d.date; });
	var dateDimGroup = dateDim.group().reduce(sumReduceAdd, sumReduceRemove, sumReduceInitial);
	
	//map
	var countryDim = cf.dimension(function(d){ return d.country.code; });
	var countryDimGroup = countryDim.group().reduce(sumReduceAdd, sumReduceRemove, sumReduceInitial);

	//create the charts
	var rowChart = dc.rowChart("#rowchart");
	rowChart.renderLabel(true)
		.width(500)
		.height(400)
		.dimension(keywordsDim)
		.group(topKeywordsGroup(10))
		.colors(d3.scale.category20())
		.elasticX(true)
		.xAxis().ticks(20)
	;
	
	var lineChart = dc.lineChart("#linechart");
	lineChart.renderLabel(true)
		.renderArea(true)
		.mouseZoomable(true)
		.height(400)
		.dimension(dateDim)
		.group(dateDimGroup)
		.transitionDuration(1000)
		.valueAccessor(function(d){
			return d.value.count;
		})
		.elasticY(true)
		.x(d3.time.scale().domain([dateDim.bottom(1)[0].date, dateDim.top(1)[0].date]))
	;
	lineChart.width(500);
	
	var mapChart = dc.geoChoroplethChart("#map");
	mapChart.width(700).height(500)
		.dimension(countryDim)
		.group(countryDimGroup)
		.colors(d3.scale.quantize().range(["#E2F2FF", "#C4E4FF", "#9ED2FF", "#81C5FF", "#6BBAFF", "#51AEFF", "#36A2FF", "#1E96FF", "#0089FF", "#0061B5"]))
			.colorDomain([0, 200])
			.colorCalculator(function (d) { return d ? mapChart.colors()(d) : '#ccc'; })
		.overlayGeoJson(countries.features, "code", function(d){
			return d.properties.postal;
		})
		.projection(d3.geo.mercator().scale(70))
	;

	dc.renderAll();
});
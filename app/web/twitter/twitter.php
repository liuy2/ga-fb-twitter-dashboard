<?php

    require 'twitter_followers.php';

    //$date is a date object
    function getTwitterData($req_date){
        //get authorization tokens
        define('KEY', '3vjUUF3WntxdDfM7NY9hdg');
        define('SECRET', 'iYAWiPgKdEd9xu9SMLJxyA1LluYj2tCtAhdd5K8Q');

        //timezone
        date_default_timezone_set('EST');

        $bearer = base64_encode(KEY . ':' . SECRET);

        $ch = curl_init();

        //access token
        curl_setopt($ch, CURLOPT_URL, 'https://api.twitter.com/oauth2/token');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $bearer,
            'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        $access_token = $result["access_token"];




        //make request with token
        //get individual tweet information from twitter
        $ch = curl_init();
        //list of tweets
        curl_setopt($ch, CURLOPT_URL, 'https://api.twitter.com/1.1/statuses/user_timeline.json?count=20&screen_name=canadaBusiness');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $access_token
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);

        //twitter's tweet info
        $data = json_decode($result, true);
        //the current number of followers on the account according to twittercounter
        $twitter_counter_data = json_decode($result_twitter_counter, true);

        //req_date -> get unix time
        $req_date_unix = strtotime($req_date);

        $return_data = json_decode('{
            "tweets": 0,
            "tweet_likes": 0,
            "retweets": 0,
            "tweet_info": []
        }', true);

        //get the date's tweets from twitter
        foreach($data as $tweet){
            //date for the tweet
            $tweet_date_str = substr($tweet["created_at"], 4, 6) . " " . substr($tweet["created_at"], 26);
            $tweet_date = strtotime($tweet_date_str);
            
            //get all the tweets that match the req_date
            if(($tweet_date - $req_date_unix) < 60 * 60 * 24 && ($tweet_date - $req_date_unix) >= 0){
                $return_data["tweets"]++;
                $return_data["tweet_likes"] += $tweet["favorite_count"];
                $return_data["retweets"] += $tweet["retweet_count"];
                $tweet_data = [];
                $tweet_data["id"] = $tweet["id_str"];
                $tweet_data["text"] = $tweet["text"];
                $tweet_data["likes"] = $tweet["favorite_count"];
                $tweet_data["retweets"] = $tweet["retweet_count"];

                
                $return_data['tweet_info'][] = $tweet_data;
            }
        }

        //get the number of followers on that date
        $return_data["en_followers"] = getDataByDate([$req_date])[$req_date]["en_followers"];

        return $return_data;
    }

    //json_encode(getTwitterData("2017-02-09"));
    echo json_encode(getTwitterData('2017-02-10'));

?>
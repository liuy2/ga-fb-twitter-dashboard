<?php

    date_default_timezone_set('EST');
    define('FILE', 'twitter_record.json');

    function writeData(){
        //POST version will have to run everyday with cron

        //access twitter
        //get authorization tokens
        define('KEY', '3vjUUF3WntxdDfM7NY9hdg');
        define('SECRET', 'iYAWiPgKdEd9xu9SMLJxyA1LluYj2tCtAhdd5K8Q');

        $bearer = base64_encode(KEY . ':' . SECRET);

        $ch = curl_init();

        //access token
        curl_setopt($ch, CURLOPT_URL, 'https://api.twitter.com/oauth2/token');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $bearer,
            'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        $access_token = $result["access_token"];


        //ENGLISH
        //make request with token
        //get individual tweet information from twitter account
        $ch = curl_init();
        //list of tweets
        curl_setopt($ch, CURLOPT_URL, 'https://api.twitter.com/1.1/statuses/user_timeline.json?count=1&screen_name=canadaBusiness');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $access_token
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $en_result = curl_exec($ch);
        curl_close($ch);

        //FRENCH
        $ch = curl_init();
        //list of tweets
        curl_setopt($ch, CURLOPT_URL, 'https://api.twitter.com/1.1/statuses/user_timeline.json?count=1&screen_name=entreprisescan');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $access_token
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $fr_result = curl_exec($ch);
        curl_close($ch);

        //twitter's tweet info
        $en_data = json_decode($en_result, true);
        $fr_data = json_decode($fr_result, true);

        //retrieve existing data
        $file_data = file_get_contents( FILE );

        $data = json_decode($file_data, true);

        $record = [];
        $record["en_followers"] = $en_data[0]["user"]["followers_count"];
        $record["fr_followers"] = $fr_data[0]["user"]["followers_count"];
        $data[date('Y-m-d', time())] = $record;

        file_put_contents( FILE, json_encode($data) );
    }

    function getDataByDate($array_of_dates){
        $data = [];

        $file_data_str = file_get_contents( FILE );
        $file_data = json_decode($file_data_str, true);

        foreach($array_of_dates as $date){
            $data[$date] = $file_data[$date];
        }

        return $data;
    }